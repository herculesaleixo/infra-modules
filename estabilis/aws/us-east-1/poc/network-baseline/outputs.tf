# VPC Outputs
output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}

output "default_route_table_id" {
  value = "${aws_vpc.vpc.default_route_table_id}"
}

# Subnet Outputs
output "public_subnet_id" {
  value = ["${aws_subnet.public.*.id}"]
}

output "private_subnet_id" {
  value = ["${aws_subnet.private.*.id}"]
}

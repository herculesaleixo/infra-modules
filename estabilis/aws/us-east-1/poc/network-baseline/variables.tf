# VPC - Variaveis do modulo
variable "vpc_cidr_block" {
  default     = "10.0.0.0/16"
  description = "Range de rede da VPC"
}

variable "instance_tenancy" {
  default     = "dedicated"
  description = "..."
}

variable "vpc_nametag" {
  type = "string"
}

# SUBNET - Variaveis do modulo
variable "subnet_nametag" {
  type = "string"
}

variable "public_subnet_count" {
  default = 1
}

variable "public_subnet_cidr_block" {
  type = "list"
}

variable "private_subnet_count" {
  default = 1
}

variable "private_subnet_cidr_block" {
  type = "list"
}

# IGW - Variaveis do modulo
variable "igw_nametag" {
  type = "string"
}

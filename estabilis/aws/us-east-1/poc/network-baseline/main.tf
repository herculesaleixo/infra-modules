resource "aws_vpc" "vpc" {
  cidr_block       = "${var.vpc_cidr_block}"
  instance_tenancy = "${var.instance_tenancy}"

  tags {
    Name = "${var.vpc_nametag}"
  }
}

resource "aws_subnet" "public" {
  count      = "${var.public_subnet_count}"
  vpc_id     = "${aws_vpc.vpc.id}"
  cidr_block = "${element("${var.public_subnet_cidr_block}", count.index)}"

  tags {
    Name = "${var.subnet_nametag}-public${count.index}"
  }
}

resource "aws_subnet" "private" {
  count      = "${var.private_subnet_count}"
  vpc_id     = "${aws_vpc.vpc.id}"
  cidr_block = "${element("${var.private_subnet_cidr_block}", count.index)}"

  tags {
    Name = "${var.subnet_nametag}-private${count.index}"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name = "${var.igw_nametag}"
  }
}